Name:     perl-constant
Version:  1.33
Release:  424
Summary:  Perl pragma to declare constants
License:  GPL-1.0-or-later OR Artistic-1.0-Perl
URL:      https://metacpan.org/release/constant
Source0:  https://cpan.metacpan.org/authors/id/R/RJ/RJBS/constant-%{version}.tar.gz
BuildArch:noarch

BuildRequires:  perl-interpreter perl-generators perl(ExtUtils::MakeMaker) perl(strict)
BuildRequires:  perl(Carp) perl(vars) perl(warnings::register)
BuildRequires:  perl(Test::More) perl(utf8) perl(warnings)
Requires: perl(Carp)

%description
This pragma allows you to declare constants at compile-time.
When a constant is used in an expression, Perl replaces it with its
value at compile time, and may then optimize the expression further.
In particular, any code in an "if (CONSTANT)" block will be optimized
away if the constant is false.

%package_help

%prep
%autosetup -n constant-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PERLLOCAL=1 NO_PACKLIST=1
%make_build

%install
%make_install
%{_fixperms} %{buildroot}/*

%check
make test

%files
%doc Changes README
%{perl_vendorlib}/*

%files help
%{_mandir}/*/*

%changelog
* Tue Jan 14 2025 Funda Wang <fundawang@yeah.net> - 1.33-424
- drop useless perl(:MODULE_COMPAT) requirement

* Thu Aug 1 2024 yanglongkang <yanglongkang@h-partners.com> - 1.33-423
- rebuild for next release

* Tue Oct 25 2022 renhongxun <renhongxun@h-partners.com> - 1.33-422
- Rebuild for next release

* Sat Dec 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.33-421
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:change mod of file

* Fri Sep 27 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.33-420
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: change the directory of README

* Fri Aug 30 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.33-419 
- Package init
